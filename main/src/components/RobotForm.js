import React, {Component} from 'react'


class RobotForm extends Component{

constructor(props){
  super(props)
  this.state={
    name:'',
    type:'',
    mass:''
  }
}

handleChange=(evt)=>{
this.setState({[evt.target.name]: evt.target.value})
}
onAdd=()=>{
this.props.onAdd({
  name: this.state.name,
  type: this.state.type,
  mass: this.state.mass
})
}
render(){

  return(<div>

              <input type="text" placeholder="name" name="name" onChange={this.handleChange} />
              <input type="text" placeholder="type" name="type" onChange={this.handleChange} />
              <input type="text" placeholder="mass" name="mass" onChange={this.handleChange} />
              <input type="button" value="add" onClick={this.onAdd} />
  </div>

)
}
}


export default RobotForm
